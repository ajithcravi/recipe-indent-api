const path = require('path');
const fs = require('fs');
const moment = require('moment');

const getRecipeIndentDetails = require('../modules/get_recipe_indent_details')
const excelConfig = require('../config/excel_skeleton.json')
const generateExcel = require('../modules/generate_excel');
const grossup = require('../config/gross_up_factor.json');
const factorOfSafety = require('../config/factor_of_safety.json');

module.exports = getRecipeIndentFile = async (recipeList, excludeList) => {
    try {
        let exceptions = []
        let rows = [excelConfig.header]

        let recipeIndent = await getRecipeIndentDetails(recipeList);

        if (recipeIndent) {
            recipeIndent.forEach(foodSource => {
                exceptions = excludeList[foodSource.main_recipe]
                if (exceptions) if (exceptions.includes(foodSource["sub_recipe"])) return;
                // let grossupFactor = grossup[foodSource.altlifelab_code] || 1;
                let fos = factorOfSafety[foodSource.procurement_category] || 0
                let currentRow = {};

                Object.keys(foodSource).forEach(parameter => {
                    if (excelConfig.skeletonVariables[parameter]) currentRow[excelConfig.skeletonVariables[parameter]] = foodSource[parameter]
                })
                // currentRow[excelConfig.skeletonVariables["gross_up"]] = grossupFactor;
                currentRow[excelConfig.skeletonVariables["fos"]] = fos;
                // currentRow[excelConfig.skeletonVariables["gross_foodsource_quantity"]] = foodSource["eff_quantity"] * grossupFactor * (1 + fos);
                currentRow[excelConfig.skeletonVariables["gross_foodsource_quantity"]] = foodSource["eff_quantity"] * (1 + fos);

                rows.push(currentRow);
            })
        }

        let fileName = `../recipe_indent_extracts/recipe_indent_${moment().format('DDMMYYYY')}.xlsx`;
        await generateExcel(rows, path.join(__dirname, fileName), 'Recipe Indent');
        return fs.readFileSync(path.join(__dirname, fileName), { encoding: 'base64' });

    } catch (err) {
        console.error(`getRecipeIndentFile error:\n ${JSON.stringify(err.message)}`);
        return false
    }
}

