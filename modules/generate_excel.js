const xlsx = require('xlsx');

/**
* Generates an excel file based on the input.
*
* @param { Object } rows Object where the key being the column names and the value being the value to be entered in the cell.
* @param { String } fileLocationAndName Location where the file has to be saved. Name of the file is also included in it.
* @param { String } sheetName Name of the sheet.
*/

module.exports = generateExcel = async (rows, fileLocationAndName, sheetName) => {
    try {
        let wb = xlsx.utils.book_new();

        let ws = xlsx.utils.json_to_sheet(rows, { skipHeader: 1 });

        xlsx.utils.book_append_sheet(wb, ws, sheetName);

        xlsx.writeFile(wb, fileLocationAndName);

        return true

    } catch (err) {
        console.error(`generateExcel error:\n ${JSON.stringify(err.message)}`);
        return false
    }
}