const path = require('path');

module.exports = client = require('graphql-client')({
    url: process.env.NMS_GRAPHQL_URL
})