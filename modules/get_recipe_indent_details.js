const client = require('./graphql_client');
const queries = require('../API/graphqlQueries')

module.exports = getRecipeIndentDetails = async (recipeList, day, mealOfTheDay, mealPlan) => {
    try {
        return new Promise(async (resolve, reject) => {

            let queryVariables = {
                "recipe_list": recipeList
            }

            const recipeIndentDetails = await client.query(queries.getRecipeIndentDetailsQuery, queryVariables).then(data => {

                let result = []

                if (data.data.getRecipeIndentDetails) {
                    data.data.getRecipeIndentDetails.forEach(mealArray => {
                        mealArray.forEach(recipeDetail => {
                            if (day && mealOfTheDay) {
                                recipeDetail["day"] = day
                                recipeDetail["meal_of_the_day"] = mealOfTheDay
                                if (mealPlan) recipeDetail["meal_plan"] = mealPlan
                            }
                            result.push(recipeDetail)
                        })
                    })
                }

                return result;
            }).catch(err => {

                console.error(`getRecipeIndentDetails error: \n ${JSON.stringify(err.message)}`);
                resolve(false);

            });
            resolve(recipeIndentDetails);
        })
        
    }catch(err) {
        console.error(`getRecipeIndentDetails error:\n ${error}`);
        return false
    }
}