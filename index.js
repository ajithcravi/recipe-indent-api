const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });
const express = require('express');
const app = express();

const routes = require('./config/routes.json')
const getRecipeIndentFile = require('./handlers/recipe_indent_extract')

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.get(routes.recipeIndent, async (req, res) => {

    try {

        let file = await getRecipeIndentFile(req.body, {})
        res.send(file)

    } catch (err) {

        console.error(`Error in hitting the route ${process.env.BASE_URL + routes.recipeIndent}:`);
        console.error(JSON.stringify(err.message));
        res.status(500).send(JSON.stringify(err.message));

    }
    
})

app.listen(process.env.PORT || 3000, () => {
    console.log(`API url: ${process.env.BASE_URL}`)
})