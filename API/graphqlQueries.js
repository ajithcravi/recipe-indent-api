module.exports = queries = {
    getRecipeIndentDetailsQuery: `
        query getRecipeIndentDetails($recipe_list: [RecipeIndentDetailInput]) {
            getRecipeIndentDetails(recipe_list: $recipe_list) {
            main_recipe
            sub_recipe
            recipe_id
            recipe_unit
            altlifelab_code
            vendor_foodsource_name
            foodsource_unit
            main_category
            sub_category
            procurement_category
            lineage
            MSS
            recipe_makes
            mss_foodsource_quantity
            mss_foodsource_quantity_per_recipe
            FSS
            Nos
            fss_recipe_quantity
            required_sub_recipe_quantity
            eff_quantity
            }
        }
        `
}